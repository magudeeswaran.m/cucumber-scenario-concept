package stepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DemoLoginSteps {
	
	static WebDriver driver = null;
	
@Given("Open the demo web shop website")
public void open_the_demo_web_shop_website() {
	driver = new ChromeDriver();
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	
}

@Then("The demo web shop home page will displayed")
public void the_demo_web_shop_home_page_will_displayed() {
	System.out.println("Home get Displayed");
}

@When("Click the login button")
public void click_the_login_button() {
	
	WebElement login = driver.findElement(By.linkText("Log in"));
	login.click();
	
	
}

@When("Enter the email in the email text field")
public void enter_the_email_in_the_email_text_field() {
	
	driver.findElement(By.id("Email")).sendKeys("shopdemo1223@gmail.com");
	
}

@When("Enter the password in the password text field")
public void enter_the_password_in_the_password_text_field() {
	
	driver.findElement(By.id("Password")).sendKeys("Demo@123");
	
}

@When("Click on the login button")
public void click_on_the_login_button() {
	
	driver.findElement(By.xpath("//input[@value='Log in']")).click();
	
}

@Then("The home page with loged user name should display")
public void the_home_page_with_loged_user_name_should_display() {
	System.out.println("Username is displaying");
}

@Then("Click the logout button")
public void click_the_logout_button() {
	
	driver.findElement(By.xpath("//a[text()='Log out']")).click();
	
}

@Then("the user should taken to the homepage of the demo web shop")
public void the_user_should_taken_to_the_homepage_of_the_demo_web_shop() {
	System.out.println("Home page demo web shop is shown");
}

@Then("close the browser")
public void close_the_browser() {
    driver.close();
}
	
}
